package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

var (
	nCol = 1
)

func usage() {
	log.Fatalf(`an - compare numerical values and get variation
usage: an <a> <b> [type] [ncol]
where type can be:
  pt: points
  pc: percentage
ncol goes from 1 to n`)
}

func compare(s1, s2 [][]string) ([]float64, error) {
	var nums1, nums2 []float64
	var result []float64

	for _, elem := range s1 {
		n := elem[nCol]
		if num, err := strconv.ParseFloat(n, 10); err == nil {
			nums1 = append(nums1, num)
		}
	}

	for _, elem := range s2 {
		n := elem[1]
		if num, err := strconv.ParseFloat(n, 10); err == nil {
			nums2 = append(nums2, num)
		}
	}

	if len(nums1) != len(nums2) {
		return []float64{}, errors.New(fmt.Sprintf("comparison list does not have the same size (%v against %v)",
			len(nums1), len(nums2)))
	}

	// Go through all elements of both slices
	for i := 0; i < len(nums1); i++ {
		max := math.Max(nums1[i], nums2[i])
		min := math.Min(nums1[i], nums2[i])
		result = append(result, max-min)
	}

	return result, nil
}

func init() {
	log.SetFlags(0)
}

func main() {
	args := os.Args
	if len(args) < 3 {
		usage()
	}

	f1, err := os.Open(args[1])
	if err != nil {
		log.Fatalf("error: %v: no such file or directory\n", args[1])
	}
	defer f1.Close()

	f2, err := os.Open(args[2])
	if err != nil {
		log.Fatalf("error: %v: no such file or directory\n", args[2])
	}
	defer f2.Close()

	// Analyze
	r1 := csv.NewReader(f1)
	s1, err := r1.ReadAll()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	r2 := csv.NewReader(f2)
	s2, err := r2.ReadAll()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	diffs, err := compare(s1, s2)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if len(os.Args) >= 5 {
		nCol, err = strconv.Atoi(os.Args[4])
		nCol--
	}

	// Display information
	nameIdx := 1
	for _, diffElem := range diffs {
		var msg string
		n1, err := strconv.ParseFloat(s1[nameIdx][nCol], 10)
		if err != nil {
			log.Fatalf("%v\n", err)
		}
		n2, err := strconv.ParseFloat(s2[nameIdx][nCol], 10)
		if err != nil {
			log.Fatalf("%v\n", err)
		}
		if n2 < n1 {
			msg = "decreased"
		} else if n2 > n1 {
			msg = "increased"
		} else {
			msg = "no change"
		}

		if len(os.Args) >= 4 {
			switch os.Args[3] {
			case "pc":
				if n2 != n1 {
					msg += fmt.Sprintf(" %.2f%%", diffElem*10)
				}
				break
			case "pt":
				pts := "points"

				if diffElem == 1 {
					pts = "point"
				}
				if n2 != n1 {
					msg += fmt.Sprintf(" %.2f %v", diffElem, pts)
				}
				break
			}
		} else { // Set percentage as default
			if n2 != n1 {
				msg += fmt.Sprintf(" %.2f%%", diffElem*10)
			}
		}

		if n2 != n1 {
			msg += fmt.Sprintf(" (%v -> %v)", s1[nameIdx][nCol], s2[nameIdx][nCol])
		}
		fmt.Printf("%v: %v\n", s2[nameIdx][0], msg)
		nameIdx++
	}
}
