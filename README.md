# an
Compare numerical values and get variation between them (from a csv file)

## building
A Go compiler is required.
```console
$ git clone https://gitlab.com/laurenL12/an.git
$ cd an/
$ ./build
```

## usage
```
an <a> <b> [type] [ncol]
```
Where type can be:
- pt: points
- pc: percentage

ncol goes from 1 to n
